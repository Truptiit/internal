<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\UserPersonalDetail;
use App\Models\UserWorkDetail;
use Carbon\Carbon;
use DataTables;
use Illuminate\Support\Str;


class AppsController extends Controller
{
  // invoice list App
  public function invoice_list()
  {
    $pageConfigs = ['pageHeader' => false];

    return view('/content/apps/invoice/invoice-list', ['pageConfigs' => $pageConfigs]);
  }

  // invoice preview App
  public function invoice_preview()
  {
    $pageConfigs = ['pageHeader' => false];

    return view('/content/apps/invoice/invoice-preview', ['pageConfigs' => $pageConfigs]);
  }

  // invoice edit App
  public function invoice_edit()
  {
    $pageConfigs = ['pageHeader' => false];

    return view('/content/apps/invoice/invoice-edit', ['pageConfigs' => $pageConfigs]);
  }

  // invoice edit App
  public function invoice_add()
  {
    $pageConfigs = ['pageHeader' => false];

    return view('/content/apps/invoice/invoice-add', ['pageConfigs' => $pageConfigs]);
  }

  // invoice print App
  public function invoice_print()
  {
    $pageConfigs = ['pageHeader' => false];

    return view('/content/apps/invoice/invoice-print', ['pageConfigs' => $pageConfigs]);
  }

  // User List Page
  
    // $pageConfigs = ['pageHeader' => false];
    // return view('/content/apps/user/app-user-list', ['pageConfigs' => $pageConfigs]);
  

  // User View Page
  public function user_view()
  {
    $pageConfigs = ['pageHeader' => false];
    return view('/content/apps/user/app-user-view', ['pageConfigs' => $pageConfigs]);
  }

  // User Edit Page
  public function user_edit()
  {
    $pageConfigs = ['pageHeader' => false];
    return view('/content/apps/user/app-user-edit', ['pageConfigs' => $pageConfigs]);
  }

  // Chat App
  public function chatApp()
  {
    $pageConfigs = [
      'pageHeader' => false,
      'contentLayout' => "content-left-sidebar",
      'pageClass' => 'chat-application',
    ];

    return view('/content/apps/chat/app-chat', [
      'pageConfigs' => $pageConfigs
    ]);
  }

  // Calender App
  public function calendarApp()
  {
    $pageConfigs = [
      'pageHeader' => false
    ];

    return view('/content/apps/calendar/app-calendar', [
      'pageConfigs' => $pageConfigs
    ]);
  }

  // Email App
  public function emailApp()
  {
    $pageConfigs = [
      'pageHeader' => false,
      'contentLayout' => "content-left-sidebar",
      'pageClass' => 'email-application',
    ];

    return view('/content/apps/email/app-email', ['pageConfigs' => $pageConfigs]);
  }
  // ToDo App
  public function todoApp()
  {
    $pageConfigs = [
      'pageHeader' => false,
      'contentLayout' => "content-left-sidebar",
      'pageClass' => 'todo-application',
    ];

    return view('/content/apps/todo/app-todo', [
      'pageConfigs' => $pageConfigs
    ]);
  }
  // File manager App
  public function file_manager()
  {
    $pageConfigs = [
      'pageHeader' => false,
      'contentLayout' => "content-left-sidebar",
      'pageClass' => 'file-manager-application',
    ];

    return view('/content/apps/fileManager/app-file-manager', ['pageConfigs' => $pageConfigs]);
  }

  // Kanban App
  public function kanbanApp()
  {
    $pageConfigs = [
      'pageHeader' => false,
      'pageClass' => 'kanban-application',
    ];

    return view('/content/apps/kanban/app-kanban', ['pageConfigs' => $pageConfigs]);
  }

  // Ecommerce Shop
  public function ecommerce_shop()
  {
    $pageConfigs = [
      'contentLayout' => "content-detached-left-sidebar",
      'pageClass' => 'ecommerce-application',
    ];

    $breadcrumbs = [
      ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['name' => "Shop"]
    ];

    return view('/content/apps/ecommerce/app-ecommerce-shop', [
      'pageConfigs' => $pageConfigs,
      'breadcrumbs' => $breadcrumbs
    ]);
  }

  // Ecommerce Details
  public function ecommerce_details()
  {
    $pageConfigs = [
      'pageClass' => 'ecommerce-application',
    ];

    $breadcrumbs = [
      ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['link' => "/app/ecommerce/shop", 'name' => "Shop"], ['name' => "Details"]
    ];

    return view('/content/apps/ecommerce/app-ecommerce-details', [
      'pageConfigs' => $pageConfigs,
      'breadcrumbs' => $breadcrumbs
    ]);
  }

  // Ecommerce Wish List
  public function ecommerce_wishlist()
  {
    $pageConfigs = [
      'pageClass' => 'ecommerce-application',
    ];

    $breadcrumbs = [
      ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['name' => "Wish List"]
    ];

    return view('/content/apps/ecommerce/app-ecommerce-wishlist', [
      'pageConfigs' => $pageConfigs,
      'breadcrumbs' => $breadcrumbs
    ]);
  }

  // Ecommerce Checkout
  public function ecommerce_checkout()
  {
    $pageConfigs = [
      'pageClass' => 'ecommerce-application',
    ];

    $breadcrumbs = [
      ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['name' => "Checkout"]
    ];

    return view('/content/apps/ecommerce/app-ecommerce-checkout', [
      'pageConfigs' => $pageConfigs,
      'breadcrumbs' => $breadcrumbs
    ]);
  }

  
  public function user_store(Request $request){
    // dd($request->all());
   try{
    $input=$request->all();

    //save data in user table
    $date = Carbon::now();
    $timeInMilliseconds = $date->valueOf();
    $input['created_at']=$timeInMilliseconds;
    $password=$request['password'];
    $input['password']=Hash::make($password);
    $user=User::create($input);


    //save data in UserPersonalDetail table
    $input['user_id']=$user->id;   
    UserPersonalDetail::create($input); 
   
     //save data in UserWorkDetail table
    unset($input['user_address']);
    UserWorkDetail::create($input);
   
    return view('content/apps/user/app-user-list');
   }catch(\Exception $e){
      echo $e->getMessage();
   }
    
  }

 


  public function user_list(Request $request)
  {
    if ($request->ajax()) {
      
      $data = User::where('user_type','!=','A');
        return Datatables::of($data)
                  ->addIndexColumn()
                  ->addColumn('status', function($row){
                      if($row->status=="Active"){
                          
                          $btn2 = '<button  data-id="'.$row->id.'" class="btn btn-success status">Active</button>';
                      }
                      else{
                        
                          $btn2 = '<button href="" data-id="'.$row->id.'" class="btn btn-danger status">Inactive</button>';
                      }
                      
                       return $btn2;
                  })
                  ->addColumn('action', function($row){
                          
                    
                         
                         
                          $content='<a href="'.url('/app/user/view/'.$row->id).'" class="btn btn-primary view">View</a><button type="button" class="btn btn-danger delete ml-1" data-id="'.$row->id.'">Delete</button><a href="'.url('/app/user/edit/'.$row->id).'" class="btn btn-primary view ml-1">Edit</a>';
                       return $content;
                   })
                  ->rawColumns(['status','action',])
                  ->make(true);
      }
      
      return view('/content/apps/user/app-user-list');
  }

  //add user
  public function user_add(){
    // $pass=Str::random(8);
      $digits    = array_flip(range('0', '9'));
    $lowercase = array_flip(range('a', 'z'));
    $uppercase = array_flip(range('A', 'Z')); 
    $special   = array_flip(str_split('!@#$%^&*()_}{[}]\|;:<>?/'));
    $combined  = array_merge($digits, $lowercase, $uppercase, $special);
    
    $pass  = str_shuffle(array_rand($digits) .
                             array_rand($lowercase) .
                             array_rand($uppercase) . 
                             array_rand($special) . 
                             implode(array_rand($combined, rand(4, 8))));
   
    return view('/content/apps/user/app-user-add',compact('pass'));
  } 

  


}
