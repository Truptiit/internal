<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPersonalDetail extends Model
{
    use HasFactory;
    protected $table='user_personal_details';
    protected $fillable=[
        'user_id',
        'user_nickname',
        'dob',
        'user_address',
        'user_alternate_email',
        'user_alternate_phone',
        'adhar_no',
        'pan_no',
        'marital_status',
        ]; 
    public $primaryKey='user_personal_details_id';
    public $timestamps = false;
}
