<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserWorkDetail extends Model
{
    use HasFactory;
    protected $table='user_work_details';
    protected $fillable=[
        'user_id',
        'date_of_joining',
        'technology',
        'user_address',
        'user_role',
        'department',
        'designation',
        
        ]; 
    public $primaryKey='user_work_details_id';


    public $timestamps = false;
}
