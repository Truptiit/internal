<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('user_lname')->after('user_fname');
            $table->integer('user_phone')->after('user_lname');
            $table->enum('user_type',array('A','U','M','O','TL'))->after('user_phone');
            $table->integer('added_by_user_id')->after('user_type');
            $table->integer('modified_by_user_id')->after('added_by_user_id');
         
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->dropSoftDeletes();
        });
    }
}
