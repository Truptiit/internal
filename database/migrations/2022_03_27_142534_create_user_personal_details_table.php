<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPersonalDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_personal_details', function (Blueprint $table) {
            $table->bigIncrements('user_personal_details_id');
            $table->integer('user_id');
            $table->string('user_nickname')->nullable();
            $table->date('dob')->nullable();
            $table->string('user_address')->nullable();
            $table->string('user_alternate_email')->nullable();
            $table->string('user_alternate_phone')->nullable();
            $table->integer('adhar_no')->nullable();
            $table->integer('pan_no')->nullable();
            $table->enum('marital_status',array('single','married'))->nullable();
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_personal_details');
    }
}
