<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserWorkDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_work_details', function (Blueprint $table) {
            $table->bigIncrements('user_work_details_id');
            $table->integer('user_id');
            $table->date('date_of_joining');
            $table->string('technology');
            $table->string('user_role');
            $table->string('department');
            $table->string('designation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_work_details');
    }
}
