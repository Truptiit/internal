<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user=new User;
        $user->user_fname="Admin";
        $user->user_lname="admin";
        $user->user_phone="9876543210";
        $user->user_type="A";
        $user->status="Active";
        $user->email="admin@yopmail.com";
        $user->password=Hash::make("admin1234");
        $user->save();
    }
}
