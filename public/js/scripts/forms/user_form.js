/*=========================================================================================
  File Name: form-validation.js
  Description: jquery bootstrap validation js
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: PIXINVENT
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

$(function() {
  jQuery.validator.addMethod("noSpace", function(value, element) { 
    return value.indexOf(" ") < 0 && value != ""; 
  }, "No space please and don't leave it empty");
  
    $("#add-new-user").validate({
        rules: {
            user_fname: {
                required:true,
                minlength:3,
                noSpace:true,
          },
          user_lname: {
            required:true,
            minlength:3,
            noSpace:true,

          },
          email : {
            required : true,
            maxlength : 30, 
            email : true
          },
          password : {
            required : true,
            minlength : 8
          },
          user_phone : {
            required : true,
            minlength : 10
          },
        },
  
        messages: {
            user_fname: {
                user_fname: "Enter more than 3 character",
          },
          user_lname: {
            user_lname: "Enter more than 3 character",
      },
          email : {
            required : 'Email is required ',
            email : 'Enter Valid Email Detail',
            maxlength : 'Email should not be more than 30 character'
          },
          password : {
            required : 'This field is required ',
            minlength : 'Password should  be at least 8 character'
          },
          user_phone: {
            required : 'This field is required ',
            minlength : 'Phone number should  be at least 10 digit'
          },
          },
  
      });
  });

  function getpassword(){
    var chars="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@#$";
    var passwordLength=8;
    var password="";
    for(var i=0;i<passwordLength;i++){
      var randomNumber=Math.floor(Math.random()* chars.length);
      password += chars.substring(randomNumber,randomNumber+1);
    }
    document.getElementById("password").value=password;
  }
//   $(function () {
//     $("#btnRemove").(function () {
//         var txtName = $("#txtName");
//         var name = $.trim(txtName.val());
//         txtName.val(name);
//     });
// });