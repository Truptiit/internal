$(function () {

  
    // All table
    var table=$('#users').DataTable({
      processing: true,
      serverSide: true,
      // ajax: "{{ url('forms/list') }}",
      ajax: base_url+"/user/list",
      columns: [
        //   {data: 'id', name: 'id',searchable:false},
          {data: 'user_fname', name: 'user_fname',searchable:false},
          {data: 'user_lname', name: 'user_lname',searchable:true},
          {data: 'email', name: 'email',searchable:true},
          {data: 'user_phone', name: 'user_phone',searchable:true},
          {data: 'user_type', name: 'user_type',searchable:true},
          {data: 'status', name: 'status',searchable:true},
          {data: 'action', name: 'action', orderable: false, searchable: false},
      ]
  });
  $('#users').on('click','.del', function(){
    // var id = $(this).data('id');
    var id = $(this).attr('data-id');
    // var badges=$(this).data('model');
    // alert(badges);
  
  // alert(id)

    bootbox.confirm({
  message: "Do You want To Delete Record",
  centerVertical: true,
  buttons: {
      confirm: {
          label: 'Yes',
          className: 'btn-success'
      },
      cancel: {
          label: 'No',
          className: 'btn-danger'
      }
  },
  callback: function (result) {
    // console.log('This was logged in the callback: ' + result);
    if(result){
     
            $.ajax({
                type:'get',
                url:'destroy/' +id,
            
                success:function(res){
                // alert(deleted);
                // table.draw();
                window.location.reload();                    
            }
            });
            }
        }
        });
    });


    $('#users').on('click','.edt', function(){
        // var id = $(this).data('id');
        var id = $(this).attr('data-id');
        alert(id);
      
      // alert(id)

        bootbox.confirm({
      message: "Do You want To change status",
      centerVertical: true,
      buttons: {
          confirm: {
              label: 'Yes',
              className: 'btn-success'
          },
          cancel: {
              label: 'No',
              className: 'btn-danger'
          }
      },
      callback: function (result) {
        // console.log('This was logged in the callback: ' + result);
        if(result){
         
          $.ajax({
            type:'get',
            url:'changestatus/' + id,
          
            success:function(res){
             
                window.location.reload();                    

            }
          });
        }
      }
  });
    });




    // $('#users').on('click','.view', function(){
    //     // var id = $(this).data('id');
    //     var id = $(this).attr('data-id');
    //     // var badges=$(this).data('model');
    //     // alert(badges);
      
    //   // alert(id)
    
    //     bootbox.confirm({
    //   message: "Do You want View?",
    //   centerVertical: true,
    //   buttons: {
    //       confirm: {
    //           label: 'Yes',
    //           className: 'btn-success'
    //       },
    //       cancel: {
    //           label: 'No',
    //           className: 'btn-danger'
    //       }
    //   },
    //   callback: function (result) {
    //     // console.log('This was logged in the callback: ' + result);
    //     if(result){
         
    //             $.ajax({
    //                 type:'get',
    //                 url:'view/' +id,
                
    //                 success:function(res){
    //                 // alert(deleted);
    //                 // table.draw();
    //                 window.location.reload();                    
    //             }
    //             });
    //             }
    //         }
    //         });
    //     });

});