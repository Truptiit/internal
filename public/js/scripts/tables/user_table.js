$(function(){
    
    var table = $('#users').DataTable({
    
        processing: true,
        serverSide: true,
        ajax: base_url+"/app/user/list",
      
        columns: [
            {data: 'user_fname', name: 'user_fname'},
            {data: 'user_lname', name: 'user_fname',searchable: false,orderable: false},
            {data: 'email', name: 'email',searchable: false,orderable: false},
            {data: 'status', name: 'status',searchable: false,orderable: false},
            {data: 'action', name: 'action',searchable: false,orderable: false},
        ]
    });

    $('#users').on('click','.delete', function(){
        var id = $(this).data('id');
        // alert(id);
        // var id = $(this).attr('data-id');
        bootbox.confirm({
            message: "Do You want To delete user",
            centerVertical: true,
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
              // console.log('This was logged in the callback: ' + result);
              if(result){
               
                $.ajax({
                    // type:'Get',
                    url:base_url+"/app/user/delete/" + id,
                   
                    success:function(res){
                      table.draw();
                    }
                  });
              }
            }
        });
       
      
    });

    $('#users').on('click','.status', function(){
        // var id = $(this).data('id');
        var id = $(this).attr('data-id');
      
      // alert(id);
        bootbox.confirm({
      message: "Do You want To change status",
      centerVertical: true,
      buttons: {
          confirm: {
              label: 'Yes',
              className: 'btn-success'
          },
          cancel: {
              label: 'No',
              className: 'btn-danger'
          }
      },
      callback: function (result) {
        // console.log('This was logged in the callback: ' + result);
        if(result){
         
          $.ajax({
           
            url:base_url+"/app/user/status/"+ id,
            // beforeSend: function(){
            //     $("#loader").show();
            //   },
            //   complete: function(){
            //     $("#loader").hide();
            //   },
            success:function(res){
             
              table.draw();
            }
          });
        }
      }
  });
    });

//     var label=document.getElementById('label').value;
// var appendHTML="<label>+label+</label>";
// $("#myform").on("submit", function(e){
//  e.preventDefault();
//  jQuery('.form-wrapper').append(appendHTML);
// })

});

$( ".reset" ).on( "click",  function() {
  $.ajax({
           
    url:base_url+"/app/user/add/",
   
    success:function(res){
     
     console.log("success");
    }
  });
});

function getpassword(){
  var chars="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@#$";
  var passwordLength=8;
  var password="";
  for(var i=0;i<passwordLength;i++){
    var randomNumber=Math.floor(Math.random()* chars.length);
    password += chars.substring(randomNumber,randomNumber+1);
  }
  document.getElementById("password").value=password;
}

// var label=document.getElementById('label').value();
// var name=document.getElementById('name').value();
// var id=document.getElementById('id').value();
// var class1=document.getElementById('class').value();
// var required=document.getElementById('required').value();

// function (){
//   var form_data=document.getElementById('form').submit();
// }
// var formEl = document.forms.myform;
// var formData = new FormData(formEl);
// var name = formData.get('name');

// function formSubmit(){
//   var label=document.getElementById('label').value;
//   var name=document.getElementById('name').value;
//   var id=document.getElementById('id').value;
//   var class1=document.getElementById('class').value;
//   var placeholder=document.getElementById('placeholder').value;
//   var required=document.getElementById('required').value;

//   var appendHTML='<label>'+label+'</label>'+'<br>'+'<input type="text" name='+name+' id='+id+' placeholder='+placeholder+' >';
//   // alert(appendHTML);
//   jQuery('.form-wrapper').append(appendHTML);
// }

$("#myform1").on("submit", function(e){
  e.preventDefault();
  var label=document.getElementById('label').value;
  var name=document.getElementById('name').value;
  var id=document.getElementById('id').value;
  var class1=document.getElementById('class').value;
  var placeholder=document.getElementById('placeholder').value;
  // console.log(placeholder);
  var required=document.getElementById('required').value;
  var appendHTML='<label>'+label+ '</label>' +(required ? '*' : '')   +'<br>'+'<input type="text" name="'+name+'" id="'+id+'" class="'+class1+'" placeholder="'+placeholder+'" '+(required ? 'required' : '') +'><br>';
  // alert(appendHTML);
  jQuery('.form-wrapper').append(appendHTML);
  $('#exampleModal1').modal('hide');

})




$("#myform2").on("submit", function(e){
  e.preventDefault();
  var label=document.getElementById('label2').value;
  var name=document.getElementById('name2').value;
  var id=document.getElementById('id2').value;
  var class1=document.getElementById('class2').value;
  var placeholder=document.getElementById('placeholder2').value;
  var required=document.getElementById('required2').value;
  var appendHTML='<label>'+label+'</label>'+'<br>'+'<textarea  name="'+name+'" id="'+id+'"  class ="'+class1+'" placeholder="'+placeholder+'" ></textarea><br>';
  // alert(appendHTML);
  jQuery('.form-wrapper').append(appendHTML);
  $('#exampleModal2').modal('hide');

})


$("#myform3").on("submit", function(e){
  e.preventDefault();
  var label=document.getElementById('label3').value;
  var name=document.getElementById('name3').value;
  var id=document.getElementById('id3').value;
  var value=document.getElementById('value3').value;
  
  // console.log(value);
 var values=value.split(" ");
 
 console.log(values[0]);
 
 for(var i=0;i<values.length;i++){
    input+='<input type="radio"  name="'+name+'" id="'+id+'"  value ="'+values[i]+'"  >'+values[i]+'  <br>';
 }
 console.log(input);
//  console.log(values);
  // var label2=document.getElementById('value').value;
  // var placeholder=document.getElementById('placeholder2').value;
  // var required=document.getElementById('required2').value;
  // var appendHTML='<label>'+label+'</label>'+'<br>'+'<input type="radio"  name="'+name+'" id="'+id+'"  value ="'+value+'"  >'+value+'  <br>';
  var appendHTML='<label>'+label+'</label>'+'<br>'+ input;
    // for(var i=0;i<values.length;i++){
    //   '<input type="radio"  name="'+name+'" id="'+id+'"  value ="'+value+'"  >'+value+'  <br>';
    // } 
  
  // alert(appendHTML);
  jQuery('.form-wrapper').append(appendHTML);
  $('#exampleModal3').modal('hide');

})


$("#myform4").on("submit", function(e){
  e.preventDefault();
  var label=document.getElementById('label4').value;
  var name=document.getElementById('name4').value;
  var id=document.getElementById('id4').value;
  var value=document.getElementById('value4').value;
  var label2=document.getElementById('value2').value;
  // var placeholder=document.getElementById('placeholder2').value;
  // var required=document.getElementById('required2').value;
  var appendHTML='<label>'+label+'</label>'+'<br>'+'<input type="checkbox"  name="'+name+'" id="'+id+'"  value ="'+value+'"  >'+label2+'  <br>';
  // alert(appendHTML);
  jQuery('.form-wrapper').append(appendHTML);
  $('#exampleModal4').modal('hide');

})
 



