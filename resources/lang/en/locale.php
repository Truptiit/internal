<?php

return [
  "Dashboards" => "Instrumententafel",
  "Organizations" => "Organizations",
  "Masters" => "Masters",
  "Departments" => "Departments",
  "Designations" => "Designations",
  "Employee Types" => "Employee Types",
  "Relationship" => "Relationship",
  "Leave Types" => "Leave Types",
  "Employees" => "Employees"
];
