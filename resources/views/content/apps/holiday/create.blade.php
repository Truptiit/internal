@extends('layouts/contentLayoutMaster')

@section('title', ' Holiday')

@section('vendor-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap4.min.css')) }}">
@endsection

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-user.css')) }}">
@endsection

@section('content')

<section id="multiple-column-form">
    <div class="row">
      <div class="col-12">
        <div class="card">
          {{-- <div class="card-header">
            <h4 class="card-title">Holiday</h4>
          </div> --}}
          <div class="card-body">
            <form class="form" action="{{url('holiday/store')}}">
                @csrf
              <div class="row">
                <div class="col-12">
                    <div class="form-group">
                      <label for="first-name-icon">Holiday Name</label>
                      <div class="input-group input-group-merge">
                       
                        <input
                          type="text"
                          id="first-name-icon"
                          class="form-control"
                          name="holiday_name"
                          placeholder="Holiday Name"
                        />
                      </div>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label for="email-id-icon">Holiday Date</label>
                      <div class="input-group input-group-merge">
                        
                        <input type="Date" name="dob" class="form-control flatpickr-inline" placeholder="YYYY-MM-DD" />
                      </div>
                    </div>
                  </div>
              </div>
              <div class="col-12">
                  <button type="submit" class="btn btn-primary mr-1">Submit</button>
                  
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</section>


@endsection



@section('vendor-script')
  {{-- Vendor js files --}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/pages/app-user-list.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
@endsection