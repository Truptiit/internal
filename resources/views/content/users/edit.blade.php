@extends('layouts/contentLayoutMaster')

@section('title', ' User')

@section('vendor-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap4.min.css')) }}">
@endsection

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-user.css')) }}">
@endsection
 @section('content')

 
{{--<section id="multiple-column-form">
    <div class="row">
      <div class="col-12">
        <div class="card">
          
          <div class="card-body">
            <form id="add_user" class="form" action="{{url('app/user/update')}}" method="POST">
              @csrf
              <div class="row">
                  <input type="hidden" name="id" value="{{$user->id}}">
                <div class="col-md-6 col-12">
                  <div class="form-group">
                    <label for="user_fname">First Name</label>
                    <input
                      type="text"
                      id="user_fname"
                      class="form-control"
                     
                      name="user_fname"
                      value="{{$user->user_fname}}"
                    />
                  </div>
                </div>
                <div class="col-md-6 col-12">
                  <div class="form-group">
                    <label for="user_lname">Last Name</label>
                    <input
                      type="text"
                      id="user_lname"
                      class="form-control"
                      placeholder="Last Name"
                      name="user_lname"
                      value="{{$user->user_lname}}"
                    />
                  </div>
                </div>
                <div class="col-md-6 col-12">
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" id="email" class="form-control" placeholder="Email" name="email" value="{{$user->email}}" />
                  </div>
                </div>
                
                <div class="col-md-6 col-12">
                  <div class="form-group">
                    <label for="user_phone">Phone</label>
                    <input
                      type="text"
                      id="user_phone"
                      class="form-control"
                      name="user_phone"
                      placeholder="Phone"
                      value="{{$user->user_phone}}"
                    />
                  </div>
                </div>
                

              </div>
              <div class="col-12">
                <button type="submit" class="btn btn-primary mr-1">Update</button>
               
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</section> --}}

<section id="multiple-column-form">
    <div class="row">
      <div class="col-12">
        <div class="card">
          
          <div class="card-body">
            <form id="add_user" class="form" action="{{url('app/user/update')}}" method="POST">
              @csrf
              <input type="hidden" name="id" value="{{$data->id}}">
              <div class="row">
                <div class="col-md-6 col-12">
                  <div class="form-group">
                    <label for="user_fname">First Name</label>
                    <input
                      type="text"
                      id="user_fname"
                      class="form-control"
                      placeholder="First Name"
                      name="user_fname"
                      value="{{$data->user_fname}}"
                    />
                  </div>
                </div>
                <div class="col-md-6 col-12">
                  <div class="form-group">
                    <label for="user_lname">Last Name</label>
                    <input
                      type="text"
                      id="user_lname"
                      class="form-control"
                      placeholder="Last Name"
                      name="user_lname"
                      value="{{$data->user_lname}}"
                    />
                  </div>
                </div>
                <div class="col-md-6 col-12">
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" id="email" class="form-control" placeholder="Email" name="email"  value="{{$data->email}}"/>
                  </div>
                </div>
                {{-- <div class="col-md-6 col-12">
                  <div class="form-group">
                    <label for="password">Password</label>
                    <div class="input-group form-password-toggle mb-2">
                    <input
                      type="password"
                      id="password"
                      class="form-control"
                      name="password"
                      placeholder="Password"
                      
                      value="{{$data->password}}"
                      />
                      <div class="input-group-append">
                        <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
                      </div>
                    </div>
                  </div>
                </div> --}}
                <div class="col-md-6 col-12">
                  <div class="form-group">
                    <label for="user_phone">Phone</label>
                    <input
                      type="text"
                      id="user_phone"
                      class="form-control"
                      name="user_phone"
                      placeholder="Phone"
                      value="{{$data->user_phone}}"
                    />
                  </div>
                </div>
                {{-- <div class="col-md-6 col-12">
                  <div class="form-group">
                    <label class="form-label" for="user_type">Select User Type</label>
                    <select id="user-plan" name="user_type" class="form-control">
                        <option value="A">A</option>
                        <option value="U">U</option>
                        <option value="M">M</option>
                        <option value="O">O</option>
                        <option value="TL">TL</option>
                    </select>
                  </div>
                </div> --}}

              </div>
              <div class="row">
              <div class="card">
                <div class="card-header">
                    <h4 class="card-title">User Personal Details</h4>
                  </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 col-12">
                          <div class="form-group">
                            <input type="hidden" name="user_personal_details_id" value="{{$data->user_personal_details_id}}">
                            <label for="user_nickname">User Nick Name</label>
                            <input
                              type="text"
                              id="user_nickname"
                              class="form-control"
                              placeholder="User Nick Name"
                              name="user_nickname"
                              value="{{$data->user_nickname}}"
                            />
                          </div>
                        </div>
                        {{-- <div class="col-md-6 col-12">
                          <div class="form-group">
                            <label>DOB</label>
                            <input type="Date" name="dob" class="form-control flatpickr-inline" placeholder="YYYY-MM-DD" />
                          </div>
                        </div> --}}
                        <div class="col-md-6 col-12">
                          <div class="form-group">
                            <label for="user_address">Address</label>
                            {{-- <input type="text" id="city-column" class="form-control" placeholder="Email" name="city-column" /> --}}
                                <input
                                class="form-control"
                                id="user_address"
                                name="user_address"
                                rows="3"
                                placeholder="Textarea"
                                value="{{$data->user_address}}"
                                >
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                          <div class="form-group">
                            <label for="user_alternate_email">Alternate Email</label>
                            <input
                              type="email"
                              id="user_alternate_email"
                              class="form-control"
                              name="user_alternate_email"
                              placeholder="Alternate Email"
                              value="{{$data->user_alternate_email}}"
                            />
                          </div>
                        </div>
                        <div class="col-md-6 col-12">
                          <div class="form-group">
                            <label for="user_alternate_phone">Alternate phone</label>
                            <input
                              type="text"
                              id="user_alternate_phone"
                              class="form-control"
                              name="user_alternate_phone"
                              placeholder="Alternate Phone"
                              value="{{$data->user_alternate_phone}}"
                            />
                          </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                              <label for="adhar_no">Adhar No</label>
                              <input
                                type="text"
                                id="adhar_no"
                                class="form-control"
                                name="adhar_no"
                                placeholder="Adhar No"
                                value="{{$data->adhar_no}}"
                              />
                            </div>
                          </div>
                          <div class="col-md-6 col-12">
                            <div class="form-group">
                              <label for="pan_no">Pan No</label>
                              <input
                                type="text"
                                id="pan_no"
                                class="form-control"
                                name="pan_no"
                                placeholder="Pan No"
                                value="{{$data->pan_no}}"
                              />
                            </div>
                          </div>
                        <div class="col-md-6 col-12">
                          <div class="form-group">
                            <label class="form-label" for="user-plan">Marital Status</label>
                            <select id="user-plan" class="form-control" name="marital_status" >
                                <option value="single">Single</option>
                                <option value="married">Married</option>
                               
                            </select>
                          </div>
                        </div>
        
                      </div>
                  </div>
              </div>
            </div>


            <div class="row">
              <div class="card">
                <div class="card-header">
                    <h4 class="card-title">User Work Details</h4>
                  </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 col-12">
                          <div class="form-group">
                            <label>Date of Joining</label>
                            <input type="hidden" name="user_work_details_id" value="{{$data->user_work_details_id}}">
                            <input type="Date" name="date_of_joining" class="form-control flatpickr-inline"   value="{{$data->date_of_joining}}" placeholder="YYYY-MM-DD" />
                          </div>
                        </div>
                        <div class="col-md-6 col-12">
                          <div class="form-group">
                            <label for="technology">Technology </label>
                            <input
                              type="text"
                              id="technology"
                              class="form-control"
                              name="technology"
                              placeholder="Technology"
                              value="{{$data->technology}}"
                            />
                          </div>
                        </div>
                        {{-- <div class="col-md-6 col-12">
                          <div class="form-group">
                            <label class="form-label" for="user_role">user_role</label>
                            <select id="user_role" class="form-control" name="user_role">
                                <option value="Front End Developer">Front End Developer</option>
                                <option value="Back End Developer">Back End Developer</option>
                                <option value="Full stack Developer">Full stack Developer</option>
                            </select>
                            </div>
                        </div> --}}
                        <div class="col-md-6 col-12">
                          <div class="form-group">
                            <label for="department">Department </label>
                            <input
                              type="text"
                              id="department"
                              class="form-control"
                              name="department"
                              placeholder="Department"
                              value="{{$data->department}}"
                            />
                          </div>
                        </div>
                        <div class="col-md-6 col-12">
                          <div class="form-group">
                            <label for="designation">Designation </label>
                            <input
                              type="text"
                              id="designation"
                              class="form-control"
                              name="designation"
                              placeholder="Designation"
                              value="{{$data->designation}}"
                            />
                          </div>
                        </div>
{{--                         
                        <label for="basic-default-password">Password</label>
                        <div class="input-group form-password-toggle mb-2">
                          <input
                            type="password"
                            class="form-control"
                            id="basic-default-password"
                            placeholder="Your Password"
                            aria-describedby="basic-default-password"
                          />
                          <div class="input-group-append">
                            <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
                          </div>
                        </div> --}}
                       
        
                      </div>
                  </div>
              </div>
            </div>
            {{-- </form> --}}
                <div class="col-12">
                  <button type="submit" class="btn btn-primary mr-1">Update</button>
                  {{-- <button type="reset" class="btn btn-outline-secondary">Reset</button> --}}
                </div>
              {{-- </div> --}}
            </form>
          {{-- </div> --}}
        </div>
      </div>
    </div>
  </section>


@endsection



@section('vendor-script')
  {{-- Vendor js files --}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/pages/app-user-list.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
@endsection