@extends('layouts/contentLayoutMaster')

@section('title', ' User')

@section('vendor-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap4.min.css')) }}">
@endsection

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-user.css')) }}">
@endsection

@section('content')


    <section id="multiple-column-form">
    <div class="row">
      <div class="col-12">
        <div class="card">
          
          <div class="card-body">
            <form   >
             
              <div class="row">
                <div class="col-md-6 col-12">
                  <div class="form-group">
                    <label for="user_fname">First Name</label>
                    <input
                      
                      class="form-control"
                      readonly="readonly"
                  value="{{$data->user_fname}}"
                     
                    />
                  </div>
                </div>
                <div class="col-md-6 col-12">
                  <div class="form-group">
                    <label for="user_lname">Last Name</label>
                    <input
                    class="form-control"
                    readonly="readonly"
                value="{{$data->user_lname}}"
                    />
                  </div>
                </div>
                <div class="col-md-6 col-12">
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input class="form-control"
                    readonly="readonly"
                value="{{$data->email}}" />
                  </div>
                </div>
                
                <div class="col-md-6 col-12">
                  <div class="form-group">
                    <label for="user_phone">Phone</label>
                    <input
                    class="form-control"
                    readonly="readonly"
                value="{{$data->user_phone}}"
                    />
                  </div>
                </div>
                

              </div>
              <div class="row">
              <div class="card">
                <div class="card-header">
                    <h4 class="card-title">User Personal Details</h4>
                  </div>
                <div class="card-body">
                    <div class="row">
                         <div class="col-md-6 col-12">
                          <div class="form-group">
                            <label for="user_nickname">User Nick Name</label>
                            <input
                              
                              class="form-control"
                              readonly="readonly"
                              value="{{$data->user_nickname}}"
                            />
                          </div>
                        </div>
                        
                        <div class="col-md-6 col-12">
                          <div class="form-group">
                            <label for="user_address">Address</label>
                           
                            <input
                              
                            class="form-control"
                            readonly="readonly"
                            value="{{$data->user_address}}"
                          />
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                          <div class="form-group">
                            <label for="user_alternate_email">Alternate Email</label>
                            <input
                            class="form-control"
                            readonly="readonly"
                value="{{$data->user_alternate_email}}"
                            />
                          </div>
                        </div>
                        <div class="col-md-6 col-12">
                          <div class="form-group">
                            <label for="user_alternate_phone">Alternate phone</label>
                            <input
                            class="form-control"
                            readonly="readonly"
                value="{{$data->user_alternate_phone}}"
                            />
                          </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                              <label for="adhar_no">Adhar No</label>
                              <input
                              class="form-control"
                              readonly="readonly"
                  value="{{$data->adhar_no}}"
                              />
                            </div>
                          </div>
                          <div class="col-md-6 col-12">
                            <div class="form-group">
                              <label for="pan_no">Pan No</label>
                              <input
                              class="form-control"
                              readonly="readonly"
                  value="{{$data->pan_no}}"
                              />
                            </div>
                          </div>
                        <div class="col-md-6 col-12">
                          <div class="form-group">
                            <label class="form-label" for="user-plan">Marital Status</label>
                            <input
                              class="form-control"
                              readonly="readonly"
                  value="{{$data->marital_status}}"
                              />
                          </div>
                        </div>
        
                      </div>
                  </div>
              </div>
            </div>


            <div class="row">
              <div class="card">
                <div class="card-header">
                    <h4 class="card-title">User Work Details</h4>
                  </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 col-12">
                          <div class="form-group">
                            <label>Date of Joining</label>
                            <input class="form-control"
                            readonly="readonly"
                value="{{$data->date_of_joining}}" />
                          </div>
                        </div>
                        <div class="col-md-6 col-12">
                          <div class="form-group">
                            <label for="technology">Technology </label>
                            <input
                            class="form-control"
                            readonly="readonly"
                value="{{$data->technology}}"
                            />
                          </div>
                        </div>
                        <div class="col-md-6 col-12">
                          <div class="form-group">
                            <label class="form-label" for="user_role">user_role</label>
                            <input
                            class="form-control"
                            readonly="readonly"
                value="{{$data->user_role}}"
                            />
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                          <div class="form-group">
                            <label for="department">Department </label>
                            <input
                            class="form-control"
                            readonly="readonly"
                value="{{$data->department}}"
                            />
                          </div>
                        </div>
                        <div class="col-md-6 col-12">
                          <div class="form-group">
                            <label for="designation">Designation </label>
                            <input
                            class="form-control"
                            readonly="readonly"
                value="{{$data->designation}}"
                            />
                          </div>
                        </div>
                        
                         
                       
        
                      </div>
                  </div>
              </div>
            </div>
            {{-- </form> --}}
                {{-- <div class="col-12">
                  <button type="submit" class="btn btn-primary mr-1">Submit</button> --}}
                  {{-- <button type="reset" class="btn btn-outline-secondary">Reset</button> --}}
                {{-- </div> --}}
              {{-- </div> --}}
            </form>
          {{-- </div> --}}
        </div>
      </div>
    </div>
  </section>

@endsection



@section('vendor-script')
  {{-- Vendor js files --}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/pages/app-user-list.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
@endsection