@extends('layouts/contentLayoutMaster')

@section('title', 'Task')

@section('vendor-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap4.min.css')) }}">
@endsection

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-user.css')) }}">
@endsection

@section('content')

<div class="row">
    <div class="col-2">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal1">
            input
          </button>
    </div>
    <div class="col-2">
        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2">textarea</button>
    </div>
    <div class="col-2">
        <button  class="btn btn-primary" data-toggle="modal" data-target="#exampleModal3">radio</button>
    </div>
    <div class="col-2">
        <button  class="btn btn-primary" data-toggle="modal" data-target="#exampleModal4">checkbox</button>
    </div>

</div>

{{-- <form class="form-wrapper">
            
</form> --}}


<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Input</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         <form id="myform1" action="" >
            <div class="form-group">
              <label for="">label</label>
              <input type="text" name="" id="label" class="form-control" placeholder="" aria-describedby="helpId">
              
            </div>
            <div class="form-group">
                <label for="">name</label>
                <input type="text" name="" id="name" class="form-control" placeholder="" aria-describedby="helpId">
                
              </div>
              <div class="form-group">
                <label for="">id</label>
                <input type="text" name="" id="id" class="form-control" placeholder="" aria-describedby="helpId">
                
              </div>
              <div class="form-group">
                <label for="">class</label>
                <input type="text" name="" id="class" class="form-control" placeholder="" aria-describedby="helpId">
                
              </div>
              <div class="form-group">
                <label for="">placeholder</label>
                <input type="text" name="" id="placeholder" class="form-control" placeholder="" aria-describedby="helpId">
                
              </div>
              <div class="form-group">
                <label for="">required</label>
                <select id="required" class="form-control" name="marital_status">
                    <option value="true">yes</option>
                    <option value="false">No</option>
                   
                </select>
                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save </button>
              </div>
         </form>
        </div>
        
      </div>
    </div>
  </div>

  <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Textarea</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         <form id="myform2" action="" >
            <div class="form-group">
              <label for="">label</label>
              <input type="text" name="" id="label2" class="form-control" placeholder="" aria-describedby="helpId">
              
            </div>
            <div class="form-group">
                <label for="">name</label>
                <input type="text" name="" id="name2" class="form-control" placeholder="" aria-describedby="helpId">
                
              </div>
              <div class="form-group">
                <label for="">id</label>
                <input type="text" name="" id="id2" class="form-control" placeholder="" aria-describedby="helpId">
                
              </div>
              <div class="form-group">
                <label for="">class</label>
                <input type="text" name="" id="class2" class="form-control" placeholder="" aria-describedby="helpId">
                
              </div>
              <div class="form-group">
                <label for="">placeholder</label>
                <input type="text" name="" id="placeholder2" class="form-control" placeholder="" aria-describedby="helpId">
                
              </div>
              <div class="form-group">
                <label for="">required</label>
                <select id="required2" class="form-control" name="marital_status">
                    <option value="yes">yes</option>
                    <option value="no">No</option>
                   
                </select>
                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save </button>
              </div>
         </form>
        </div>
        
      </div>
    </div>
  </div>


  <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Radio Button</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         <form id="myform3" action="" >
            <div class="form-group">
              <label for="">label</label>
              <input type="text" name="" id="label3" class="form-control" placeholder="" aria-describedby="helpId">
              
            </div>
            <div class="form-group">
                <label for="">name</label>
                <input type="text" name="" id="name3" class="form-control" placeholder="" aria-describedby="helpId">
                
              </div>
              <div class="form-group">
                <label for="">id</label>
                <input type="text" name="" id="id3" class="form-control" placeholder="" aria-describedby="helpId">
                
              </div>
              <div class="form-group">
                <label for="">value</label>
                <input type="text" name="" id="value3" class="form-control" placeholder="" aria-describedby="helpId">
                
              </div>

              {{-- <div class="form-group">
                <label for="">label2</label>
                <input type="text" name="" id="value" class="form-control" placeholder="" aria-describedby="helpId">
                
              </div> --}}

              {{-- <div class="form-group">
                <label for="">placeholder</label>
                <input type="text" name="" id="placeholder2" class="form-control" placeholder="" aria-describedby="helpId">
                
              </div>
              <div class="form-group">
                <label for="">required</label>
                <select id="required2" class="form-control" name="marital_status">
                    <option value="yes">yes</option>
                    <option value="no">No</option>
                   
                </select>
                
              </div> --}}
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save </button>
              </div>
         </form>
        </div>
        
      </div>
    </div>
  </div>


  <div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Check box</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         <form id="myform4" action="" >
            <div class="form-group">
              <label for="">label</label>
              <input type="text" name="" id="label4" class="form-control" placeholder="" aria-describedby="helpId">
              
            </div>
            <div class="form-group">
                <label for="">name</label>
                <input type="text" name="" id="name4" class="form-control" placeholder="" aria-describedby="helpId">
                
              </div>
              <div class="form-group">
                <label for="">id</label>
                <input type="text" name="" id="id4" class="form-control" placeholder="" aria-describedby="helpId">
                
              </div>
              <div class="form-group">
                <label for="">value</label>
                <input type="text" name="" id="value4" class="form-control" placeholder="" aria-describedby="helpId">
                
              </div>

              <div class="form-group">
                <label for="">label2</label>
                <input type="text" name="" id="value2" class="form-control" placeholder="" aria-describedby="helpId">
                
              </div>

              {{-- <div class="form-group">
                <label for="">placeholder</label>
                <input type="text" name="" id="placeholder2" class="form-control" placeholder="" aria-describedby="helpId">
                
              </div>
              <div class="form-group">
                <label for="">required</label>
                <select id="required2" class="form-control" name="marital_status">
                    <option value="yes">yes</option>
                    <option value="no">No</option>
                   
                </select>
                
              </div> --}}
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save </button>
              </div>
         </form>
        </div>
        
      </div>
    </div>
  </div>
 
<div class="row">
  <div class=" col-6 card">
    <form class="form-wrapper">
       
    </form>
   </div>

   <div class=" col-6 card">
    <form class="form-wrapper1">
        Hello
    </form>
   </div>
</div>
 

<section id="analytics-card">
    {{-- <div class="row match-height"> --}}
      {{-- <form class="form-wrapper">
        <div class="col-lg-6 col-12">
            <input type="hidden" name="name[]" value="">
            {{-- <input type="text" name="name[]" value="">
            <button class=" btn btn-primary add-button" title="Add field">Add More</button> --}}
        {{-- </div> --}}
    {{-- </form> --}} 
    
      
      

      {{-- <div class="col-lg-6 col-12">
        <div class="card">
            <div class="card-body">
                <div class="row pb-50">
                  <div class="col-lg-6 col-12 d-flex justify-content-between flex-column order-lg-1 order-2 mt-1 mt-lg-0">
                    <div class="mb-1 mb-lg-0">
                      <h2 class="font-weight-bolder mb-25">2.7K</h2>
                      <p class="card-text font-weight-bold mb-2">Avg Sessions</p>
                      <div class="font-medium-2">
                        <span class="text-success mr-25">+5.2%</span>
                        <span>vs last 7 days</span>
                      </div>
                    </div>
                    <button type="button" class="btn btn-primary">View Details</button>
                  </div>
                  <div class="col-lg-6 col-12 d-flex justify-content-between flex-column text-right order-lg-2 order-1">
                    <div class="dropdown chart-dropdown">
                      <button
                        class="btn btn-sm border-0 dropdown-toggle p-50"
                        type="button"
                        id="dropdownItem5"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                      >
                        Last 7 Days
                      </button>
                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownItem5">
                        <a class="dropdown-item" href="javascript:void(0);">Last 28 Days</a>
                        <a class="dropdown-item" href="javascript:void(0);">Last Month</a>
                        <a class="dropdown-item" href="javascript:void(0);">Last Year</a>
                      </div>
                    </div>
                    <div id="avg-session-chart"></div>
                  </div>
                </div>
                <hr />
                <div class="row avg-sessions pt-50">
                  <div class="col-6 mb-2">
                    <p class="mb-50">Goal: $100000</p>
                    <div class="progress progress-bar-primary" style="height: 6px">
                      <div
                        class="progress-bar"
                        role="progressbar"
                        aria-valuenow="50"
                        aria-valuemin="50"
                        aria-valuemax="100"
                        style="width: 50%"
                      ></div>
                    </div>
                  </div>
                  <div class="col-6 mb-2">
                    <p class="mb-50">Users: 100K</p>
                    <div class="progress progress-bar-warning" style="height: 6px">
                      <div
                        class="progress-bar"
                        role="progressbar"
                        aria-valuenow="60"
                        aria-valuemin="60"
                        aria-valuemax="100"
                        style="width: 60%"
                      ></div>
                    </div>
                  </div>
                  <div class="col-6">
                    <p class="mb-50">Retention: 90%</p>
                    <div class="progress progress-bar-danger" style="height: 6px">
                      <div
                        class="progress-bar"
                        role="progressbar"
                        aria-valuenow="70"
                        aria-valuemin="70"
                        aria-valuemax="100"
                        style="width: 70%"
                      ></div>
                    </div>
                  </div>
                  <div class="col-6">
                    <p class="mb-50">Duration: 1yr</p>
                    <div class="progress progress-bar-success" style="height: 6px">
                      <div
                        class="progress-bar"
                        role="progressbar"
                        aria-valuenow="90"
                        aria-valuemin="90"
                        aria-valuemax="100"
                        style="width: 90%"
                      ></div>
                    </div>
                  </div>
                </div>
              </div>
        </div>
      </div> --}}
    {{-- </div> --}}
    </div>
{{-- </section> --}}



</div>


@endsection

@section('vendor-script')
  {{-- Vendor js files --}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
@endsection

@section('page-script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
  {{-- Page js files --}}
  {{-- <script src="{{ asset(mix('js/scripts/pages/app-user-list.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/tables/table-datatables-basic.js')) }}"></script> --}}
  <script src="{{ asset(mix('js/scripts/tables/user_table.js')) }}"></script>
  {{-- <script src="{{ asset('js/task/cutom.js') }}"></script> --}}
  
@endsection